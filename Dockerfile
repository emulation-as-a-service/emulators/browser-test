from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="browser2"
LABEL "EAAS_EMULATOR_VERSION"="x"

run sed -i.bak "/^# deb .*partner/ s/^# //" /etc/apt/sources.list 

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
unzip \
chromium-browser \
adobe-flashplugin \
vde2 \
libvde-dev libvdeplug-dev libvdeplug2 libvde0 \
fonts-liberation \
    fonts-freefont-ttf \
    fonts-dejavu \
    fonts-arphic-ukai fonts-arphic-uming fonts-ipafont-mincho fonts-ipafont-gothic fonts-unfonts-core \
    fonts-indic \
libgtk-3-0 libasound2 libdbus-glib-1-2 libnss3-tools \
    adobe-flashplugin software-properties-common libavcodec-extra inotify-tools

RUN wget https://download-installer.cdn.mozilla.net/pub/firefox/releases/49.0.1/linux-x86_64/en-US/firefox-49.0.1.tar.bz2 && \
    tar xvf firefox-49.0.1.tar.bz2
RUN mv ./firefox /opt/firefox

add browser.sh /usr/bin/browser.sh
run mkdir /libexec \
  && curl -o /libexec/eaas-proxy-download.zip -L "https://gitlab.com/emulation-as-a-service/eaas-proxy/-/jobs/artifacts/master/download?job=build" \
  && cd /libexec \
  && unzip eaas-proxy-download.zip \
  && mv eaas-proxy/eaas-proxy vdenode \
  && rm -r eaas-proxy \
  && mv vdenode eaas-proxy \
  && chmod +x eaas-proxy \
  && rm eaas-proxy-download.zip

run cd /tmp
run curl -L -o google-chrome-stable_53.0.2785.143-1_amd64.deb https://github.com/oldweb-today/browser-chrome/raw/master/deb/google-chrome-stable_53.0.2785.143-1_amd64.deb
run curl -L -o libpepflashplayer.so https://github.com/oldweb-today/browser-chrome/raw/master/flash/libpepflashplayer.so

run dpkg -i google-chrome-stable_53.0.2785.143-1_amd64.deb; apt-get update; apt-get install -fqqy && \
    rm -rf /var/lib/opts/lists/*

run mkdir app
run mv libpepflashplayer.so /app/libpepflashplayer.so
COPY policy.json /etc/opt/chrome/policies/managed/policy.json

run mkdir metadata
