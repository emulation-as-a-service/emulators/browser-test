#!/bin/bash

#crome
mkdir ~/.config/
mkdir ~/.config/google-chrome
touch ~/.config/google-chrome/First\ Run

EAAS_PROXY_READY_BASE=/tmp/eaas-proxy.run
mkdir -p "$EAAS_PROXY_READY_BASE"

if [ ! -d "$EAAS_PROXY_READY_BASE" ]; then
	echo "failed creating EAAS_PROXY_READY_BASE"
	exit 1
fi

export EAAS_PROXY_READY_PATH="$EAAS_PROXY_READY_BASE/run"
/libexec/eaas-proxy 8090 "/emucon/data/networks/nic_$NIC" $MAC dhcp socks5 & 

while ! test -f "$EAAS_PROXY_READY_PATH"; do
  inotifywait --timeout 10 --event create "$EAAS_PROXY_READY_BASE" 
done

FLASH=/app/libpepflashplayer.so
google-chrome --no-default-browser-check --no-sandbox \
  --ppapi-flash-path=${FLASH} \
  --allow-outdated-plugins \
  --always-authorize-plugins \
  --allow-hidden-media-playback \
  --disable-popup-blocking \
  --disable-background-networking \
  --disable-background-timer-throttling \
  --disable-client-side-phishing-detection \
  --disable-default-apps \
  --disable-hang-monitor \
  --disable-prompt-on-repost \
  --disable-domain-reliability \
  --disable-renderer-backgrounding \
  --disable-infobars \
  --metrics-recording-only \
  --no-first-run \
  --translate-ranker-model-url=about:blank \
  --safebrowsing-disable-auto-update \
  --autoplay-policy=no-user-gesture-required "$@"
